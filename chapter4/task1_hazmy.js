const Car = function (make, speed) {
    this.make = make;
    this.speed = speed;
};

Car.prototype.accelerate = function (accelerate) {
    this.speed += accelerate
    console.log(`${this.make} is going at ${this.speed} km/h`);
};

Car.prototype.brake = function (brake) {
    this.speed -= brake
    console.log(`${this.make} is going at ${this.speed} km/h`);
};

let bmw = new Car('bmw', 0)
let mercedes = new Car('mercedes', 0)

bmw.accelerate(130);
mercedes.accelerate(105);
bmw.brake(5);
mercedes.brake(5);